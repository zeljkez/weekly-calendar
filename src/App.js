import CalendarInitializer from "./components/calendar/calendarInitializer";

function App() {
  return (
    <div>
      <CalendarInitializer />
    </div>
  );
}

export default App;
