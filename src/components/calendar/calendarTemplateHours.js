import styles from "./calendarTemplateHours.module.css";

const CalendarTemplateHours = () => {
  let hours = [];

  for (let i = 8; i < 20; i++) {
    for (let j = 0; j < 2; j++) {
      if (i === 19 && j === 1) break;
      hours.push(i + ":" + (j === 0 ? "00" : 30 * j));
    }
  }

  return (
    <div>
      {hours.map((hour, index) => {
        return (
          <div key={index} className={styles.cellBottomBorder}>
            <div className={styles.calendarHour}>{hour}</div>
          </div>
        );
      })}
    </div>
  );
};

export default CalendarTemplateHours;
