import CalendarDays from "./calendarDays";
import NewAppointment from "../appointment/newAppointment";
import AppointmentMessage from "../appointment/appointmentMessage";
import { useState } from "react";
import { flatten, filter, findIndex } from 'underscore'

const Calendar = (props) => {

  const [isModalOpened, setIsModalOpened] = useState(false);
  const [showMessage, setShowMessage] = useState(false);
  const [message, setMessage] = useState("");
  const [appointmentData, setAppointmentData] = useState({
    dateTime: null,
    dayIndex: null,
    appointmentIndex: null
  });

  const openAppointmentScheduler = (appointmentData) => {
    setAppointmentData(() => {
      return {
        dateTime: appointmentData.dateTime,
        dayIndex: appointmentData.dayIndex,
        appointmentIndex: appointmentData.appointmentIndex
      };
    });

    setIsModalOpened(true);
  };

  const closeAppointmentScheduler = () => {
    setIsModalOpened(false);
  };

  const closeMessageContainer = () => {
    setShowMessage(false);
  };

  const setAppointmentAsBusy = (appointment, patient) => {
    appointment.isBusy = true;
    appointment.scheduledBy = patient;
  };


  const saveNewAppointment = (event) => {
    const flattened = flatten(props.appointments);

    const filtered = filter(flattened, (el) => el.scheduledBy && el.scheduledBy.mbo === event.patient.mbo);

    if (filtered.length === 2) {
      setMessage("It's possible to schedule only 2 appointments per week.");
    }
    else if (filtered.length === 1) {
      const dayAppointments = props.appointments[appointmentData.dayIndex];

      if (findIndex(dayAppointments, (appointment) => appointment.scheduledBy && appointment.scheduledBy.mbo === event.patient.mbo) < 0) {
        setAppointmentAsBusy(dayAppointments[appointmentData.appointmentIndex], event.patient);
        setMessage("New appointment scheduled.");
      }
      else {
        setMessage("It's possible to schedule only 1 appointment per day.");
      }
    }
    else {
      const dayAppointments = props.appointments[appointmentData.dayIndex];
      setAppointmentAsBusy(dayAppointments[appointmentData.appointmentIndex], event.patient);
      setMessage("New appointment scheduled.");
    }

    setIsModalOpened(false);
    setShowMessage(true);
  };

  return (
    <div>
      <CalendarDays days={props.days} appointments={props.appointments} onAppointmentSelected={openAppointmentScheduler} />

      {isModalOpened && (
        <NewAppointment
          onCancel={closeAppointmentScheduler}
          onSave={saveNewAppointment}
          selectedDateTime={appointmentData.dateTime}
        />
      )}

      {showMessage && (<AppointmentMessage onClose={closeMessageContainer} message={message} />)}
    </div>
  );
};

export default Calendar;
