import Calendar from "./calendar";
import { getHour } from '../utils/utility';
import { getWeekdayShort } from '../utils/utility';
import { getWeekdayShortFromDate } from '../utils/utility';
import { createWeekDays } from '../utils/weekDayGenerator';
import { random } from 'underscore'

const CalendarInitializer = () => {
    const saturday = getWeekdayShort(6);
    const sunday = getWeekdayShort(7);

    const morningBreakTime = getHour(11);
    const afternoonBreakTime = getHour(16);
    const morningShiftEndTime = getHour(14);
    const afternoonShiftStartTime = getHour(13);

    const calendarDays = [];
    let appointments = [];
    const appointmentsPerDay = [];

    const isEvenDay = (date) => {
        return date.getDate() % 2 === 0;
    }

    const isOutOfWorkingHours = (date) => {
        const hour = date.getHours();
        const minutes = date.getMinutes();

        if (getWeekdayShortFromDate(date) === sunday)
            return true;

        if (getWeekdayShortFromDate(date) === saturday && !isEvenDay(date))
            return true;

        return isEvenDay(date)
            ? hour > afternoonShiftStartTime || (hour === morningShiftEndTime && minutes !== 0)
            : hour < afternoonShiftStartTime;
    };

    const isBreakTime = (date) => {
        const hour = date.getHours();
        const minutes = date.getMinutes();

        return (isEvenDay(date) && hour === morningBreakTime && minutes === 0) || (!isEvenDay(date) && hour === afternoonBreakTime && minutes === 0);
    };

    const calculateHours = (day) => {
        appointments = [];

        for (let i = 8; i < 20; i++) {
            for (let j = 0; j < 2; j++) {
                if (i === 19 && j === 1) break;

                const dateTime = new Date(
                    day.year,
                    day.monthNumber,
                    day.date
                );

                dateTime.setHours(i);
                dateTime.setMinutes(j === 0 ? 0 : 30);

                appointments.push({
                    dateTime: dateTime,
                    isBusy: false,
                    isOutOfWorkingHours: isOutOfWorkingHours(dateTime),
                    isBreakTime: isBreakTime(dateTime),
                    scheduledBy: null
                });
            }
        }
    };

    const setRandomAppointments = (appointmentsPerDay) => {
        const randomAppointments = [];

        while (randomAppointments.length < 15) {
            const appointmentList = appointmentsPerDay[random(0, appointmentsPerDay.length - 1)];
            const randomAppointment = appointmentList[random(0, appointmentList.length - 1)];

            if (!randomAppointment.isBusy && !randomAppointment.isBreakTime && !randomAppointment.isOutOfWorkingHours) {
                randomAppointment.isBusy = true;
                randomAppointments.push(randomAppointment);
            }
        };
    };

    createWeekDays(calendarDays);

    calendarDays.forEach((day) => {
        calculateHours(day);
        appointmentsPerDay.push(appointments);
    });

    setRandomAppointments(appointmentsPerDay);

    return (
        <Calendar days={calendarDays} appointments={appointmentsPerDay} />
    )
};

export default CalendarInitializer;