import styles from "./calendarHours.module.css";

const CalendarHours = (props) => {
  function setAppointmentHandler(appointmentData) {
    props.onAppointmentSelected(appointmentData);
  };

  return (
    <div>
      {props.appointments.map((appointment, index) => {
        const dateTime = appointment.dateTime.toString();
        return (
          <div
            key={index}
            className={`${styles.cellBottomBorder} 
            ${appointment.isOutOfWorkingHours && styles.appointmentUnavailable}
            ${!appointment.isOutOfWorkingHours && styles.appointmentAvailable}
            ${appointment.isBusy && styles.appointmentBusy}
            ${appointment.isBreakTime && styles.breakTime} `}
            value={dateTime}
            onClick={() => setAppointmentHandler({ dateTime: dateTime, dayIndex: props.dayIndex, appointmentIndex: index })}>
          </div>
        );
      })}
    </div>
  );
};

export default CalendarHours;
