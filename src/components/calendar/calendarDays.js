import styles from "./calendarDays.module.css";
import CalendarHours from "./calendarHours";
import CalendarTemplateHours from "./calendarTemplateHours";

const CalendarDays = (props) => {

  const onAppointmentSelectedHandler = (appointmentData) => {
    props.onAppointmentSelected(appointmentData);
  };

  return (
    <div className={styles.calendarTable}>
      <div className={styles.calendarDayColumn}>
        <div className={styles.calendarDayName}>&nbsp;</div>
        <CalendarTemplateHours />
      </div>
      {props.days.map((day, index) => {
        return (
          <div className={styles.calendarDayColumn} key={index}>
            <div className={styles.calendarDayName}>{`${day.dayName}, ${day.monthName} ${day.date}`}</div>
            <CalendarHours
              dayIndex={index}
              appointments={props.appointments[index]}
              onAppointmentSelected={onAppointmentSelectedHandler}
            />
          </div>
        );
      })}
    </div>
  );
};

export default CalendarDays;
