export const createWeekDays = (calendarDays) => {
    for (let i = 1; i < 8; i++) {
        const dayOfWeek = new Date();
        dayOfWeek.setDate(dayOfWeek.getDate() + i);

        const dayOfWeekYear = dayOfWeek.getFullYear();

        const dayOfWeekMonth = dayOfWeek.toLocaleString("default", {
            month: "short",
        });
        const dayOfWeekMonthNumber = dayOfWeek.getMonth();
        const dayOfWeekDate = dayOfWeek.getDate();

        const dayOfWeekName = dayOfWeek.toLocaleString("default", {
            weekday: "short",
        });

        calendarDays.push({
            year: dayOfWeekYear,
            monthName: dayOfWeekMonth,
            monthNumber: dayOfWeekMonthNumber,
            date: dayOfWeekDate,
            dayName: dayOfWeekName,
        });
    }
};