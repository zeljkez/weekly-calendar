export const getWeekdayShort = (dayNumber) => new Date(0, 0, dayNumber).toLocaleDateString("default", { weekday: "short" });
export const getWeekdayShortFromDate = (date) => date.toLocaleString("default", { weekday: "short" });
export const getHour = (hour) => new Date(0, 0, 0, hour).getHours();