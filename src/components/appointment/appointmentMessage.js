import styles from "./appointmentMessage.module.css";

const AppointmentMessage = (props) => {
    return (
        <div>
            <div className={styles.overlay}></div>
            <div className={styles.modal}>
                <p>{props.message}</p>
                <div className={styles.actionButton}>
                    <button type="button" onClick={props.onClose}>
                        Close
                    </button>
                </div>
            </div>
        </div>
    );
};

export default AppointmentMessage;