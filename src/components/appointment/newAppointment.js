import styles from "./newAppointment.module.css";
import { useState } from "react";
import Moment from 'react-moment';

const NewAppointment = (props) => {
  const mboNumberLength = 9;

  const [patient, setPatientData] = useState({
    name: null,
    lastName: null,
    email: null,
    contact: null,
    mbo: null
  });

  const [isValid, setIsValid] = useState(true);

  const nameChangeHandler = (event) => {
    setPatientData((prevState) => {
      return { ...prevState, name: event.target.value };
    });
  };

  const lastNameChangeHandler = (event) => {
    setPatientData((prevState) => {
      return { ...prevState, lastName: event.target.value };
    });
  };

  const emailChangeHandler = (event) => {
    setPatientData((prevState) => {
      return { ...prevState, email: event.target.value };
    });
  };

  const contactChangeHandler = (event) => {
    setPatientData((prevState) => {
      return { ...prevState, contact: event.target.value };
    });
  };

  const mboChangeHandler = (event) => {
    setPatientData((prevState) => {
      return { ...prevState, mbo: event.target.value };
    });
  };

  const onSubmitHandler = (event) => {
    event.preventDefault();

    if (!patient.name || !patient.lastName || !patient.email || !patient.contact || !patient.mbo || patient.mbo.length !== mboNumberLength) {
      setIsValid(false);
      return;
    }

    props.onSave({ date: props.selectedDateTime, patient: patient })
  };

  return (
    <div>
      <div className={styles.overlay}></div>
      <div className={styles.modal}>
        <form onSubmit={onSubmitHandler}>
          <div className={styles.appointmentData}>
            <div className={styles.dateTimeValue}>
              <Moment format="MMM DD YYYY HH:mm">{props.selectedDateTime}</Moment>
            </div>
            <div>
              <label>Name</label>
              <input style={{ borderColor: !patient.name && !isValid ? 'red' : 'black' }} type="text" onChange={nameChangeHandler} />
            </div>
            <div>
              <label>Last Name</label>
              <input style={{ borderColor: !patient.lastName && !isValid ? 'red' : 'black' }} type="text" onChange={lastNameChangeHandler} />
            </div>
            <div>
              <label>Email</label>
              <input style={{ borderColor: !patient.email && !isValid ? 'red' : 'black' }} type="email" onChange={emailChangeHandler} />
            </div>
            <div>
              <label>Contact</label>
              <input style={{ borderColor: !patient.contact && !isValid ? 'red' : 'black' }} type="number" onChange={contactChangeHandler} />
            </div>
            <div>
              <label>MBO</label>
              <input style={{ borderColor: ((patient.mbo && patient.mbo.length !== mboNumberLength) || !patient.mbo) && !isValid ? 'red' : 'black' }} type="number" onChange={mboChangeHandler} />
            </div>
            <div className={styles.actionButtons}>
              <button type="button" onClick={props.onCancel}>
                Cancel
              </button>
              <button type="submit">
                Save
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default NewAppointment;
